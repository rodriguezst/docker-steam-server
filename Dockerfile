FROM archlinux/archlinux:base-devel

# Add multilib repository to run 32-bit applications on 64-bit installs
RUN echo -e " \n\
[multilib] \n\
Include = /etc/pacman.d/mirrorlist \n\
" >> /etc/pacman.conf

# Set locale
RUN sed -e "s/#en_US\.UTF-8/en_US\.UTF-8/" -i /etc/locale.gen \
  && echo -e "LANG=en_US.UTF-8" > /etc/locale.conf \
  && locale-gen

# Add user and workdir
ARG user=docker
RUN useradd -m $user \
  && echo "$user ALL=(ALL:ALL) NOPASSWD:ALL" > /etc/sudoers.d/$user
USER $user
WORKDIR /home/$user

# Install yay
RUN sudo pacman -Syu --needed --noconfirm git \
  && git clone https://aur.archlinux.org/yay.git \
  && cd yay \
  && makepkg -sri --needed --noconfirm \
  && cd \
  # Clean up
  && rm -rf .cache yay

# Install Steam
RUN sudo pacman -Sy --noconfirm gnu-free-fonts lib32-vulkan-intel vulkan-intel steam

# Install Lutris dependencies
RUN sudo pacman -S --noconfirm wine-staging giflib lib32-giflib libpng lib32-libpng \
  libldap lib32-libldap gnutls lib32-gnutls mpg123 lib32-mpg123 openal \
  lib32-openal v4l-utils lib32-v4l-utils libpulse lib32-libpulse libgpg-error \
  lib32-libgpg-error alsa-plugins lib32-alsa-plugins alsa-lib lib32-alsa-lib \
  libjpeg-turbo lib32-libjpeg-turbo sqlite lib32-sqlite libxcomposite \
  lib32-libxcomposite libxinerama lib32-libgcrypt libgcrypt lib32-libxinerama \
  ncurses lib32-ncurses opencl-icd-loader lib32-opencl-icd-loader libxslt \
  lib32-libxslt libva lib32-libva gtk3 lib32-gtk3 gst-plugins-base-libs \
  lib32-gst-plugins-base-libs vulkan-icd-loader lib32-vulkan-icd-loader

# Install packages (Proton, Microsoft Fonts, Lutris)
RUN yay -Sc --noconfirm proton-ge-custom-bin ttf-ms-win10 lutris
